#include <stdlib.h>
#include <stdio.h>

#include "lexer.h"


struct lexer_context *lexer_context_create(int (*lookahead)(void*, char*), void (*consume)(void*), void* user_data) {
    struct lexer_context *ret = NULL;

    ret = (struct lexer_context*)malloc(sizeof(struct lexer_context));

    ret->states = NULL;
    ret->num_states = 0;

    ret->line = 0;
    ret->column = 0;

    ret->lookahead = lookahead;
    ret->consume = consume;
    ret->user_data = user_data;

    return ret;
}

int lexer_define_tokens(struct lexer_context *context, struct re_token_definition *definitions) {
    return re_compile_tokens(definitions, &context->states, &context->num_states);
}

int lexer_next_token(struct lexer_context *context, struct lexer_token *token) {
    int ret = -1;
    struct re_fsm_state *states = context->states;
    int current_state = 0;
    char c;
    int i, eof = 0;
    char matched[2048];
    int k = 0;
    int start_line = context->line, start_column = context->column;

    if (states == NULL) {
        return -1;
    }

    while (1) {
        if (context->lookahead(context->user_data, &c) != 0) {
            eof = 1;
            c = EOF;
            break;
        }

        if (states[current_state].next_state[(int)c] != -1) {
            current_state = states[current_state].next_state[(int)c];
            context->consume(context->user_data);
            matched[k++] = c;

            context->column++;
            if (c == '\n') {
                context->line++;
                context->column = 0;
            }
            continue;
        }
        else {
            break;
        }
    }

    /* Good end, or error ? */
    if (eof && current_state == 0) {
        /* EOF token */
        token->id = LEXER_TOKEN_EOF;
        token->content = NULL;
        token->line = start_line;
        token->column = start_column;

        ret = 0;
    }
    else {
        i = states[current_state].final_id;

        /* If at least something was matched, return a token; no lambda tokens */
        if (k > 0 && i != -1) {
            matched[k++] = '\0';

            /* A token */
            token->id = i;
            token->content = matched;
            token->line = start_line;
            token->column = start_column;

            ret = 0;
        }
        else {
            /* An error */
            fprintf(stderr, "Parser error. Unexpected character <%c> %02x in state %d. ", c, c, current_state);
            fprintf(stderr, "Expecting: ");
            for (i = 0; i < 256; i++) {
                if (states[current_state].next_state[i] != -1) {
                    fprintf(stderr, "<%c> %02x ", i, i);
                }
            }
            fprintf(stderr, "\n");
        }
    }

    return ret;
}

