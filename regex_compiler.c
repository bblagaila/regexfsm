#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "regex_compiler.h"


// #define REGEX_COMPILER_DEBUG 1


#ifdef REGEX_COMPILER_DEBUG
#define DEBUG                   1
#endif

#define REL_TERM    0
#define REL_OP      1
#define REL_SYM     2

struct relem {
    char type;
    union {
        char op;
        char sym;
        int id;
    };
};

#define RELEM_GROW_AMOUNT 10

struct relements {
    int length;
    int size;
    struct relem *elements;
};

struct re_tree {
    struct relem element;
    struct re_tree *left, *right;

    int id;
    int number;
};

struct re_node_attr {
    int leaf_number;
    int nullable;
    struct relem symbol;
    int *firstpos;
    int *lastpos;
    int final_id;
};

struct re_stack {
    void **stack;
    int level;
};

/* Regex compiler context */
struct re_cc {
    struct relements *re_elements;
    struct re_tree *re_tree;
    struct re_node_attr *node_attr;
    int num_nodes;
    int num_leafs;
    int **followpos;

    /* Output */
    struct re_fsm_state *fsm;
    int num_states;

    /* Error handling */
    int error;
};


/* Private methods */

void stack_init(struct re_stack *stack, int size) {
    stack->stack = (void*)malloc((size + 1) * sizeof(void*));
    stack->level = -1;
}

void stack_push(struct re_stack *stack, void *value) {
    stack->stack[++stack->level] = value;
}

void *stack_pop(struct re_stack *stack) {
    return stack->stack[stack->level--];
}

void *stack_peek(struct re_stack *stack) {
    return stack->stack[stack->level];
}

int stack_level(struct re_stack *stack) {
    return stack->level + 1;
}

void stack_destroy(struct re_stack *stack) {
    free(stack->stack);
    stack->stack = NULL;
}

void** matrix_allocate(size_t rows, size_t columns, size_t element_size) {
    void **ret = NULL;
    int i;

    ret = (void**)malloc(rows * sizeof(void*));
    for (i = 0; i < columns; i++) {
        ret[i] = (void*)malloc(columns * element_size);
    }

    return ret;
}

void matrix_destroy(void **matrix, size_t rows, size_t columns) {
    int i;

    for (i = 0; i < columns; i++) {
        free(matrix[i]);
    }

    free(matrix);
}

struct relements* create_relements(int size) {
    struct relements *ret = NULL;

    ret = (struct relements*)malloc(sizeof(struct relements));
    ret->length = 0;
    ret->size = size;
    ret->elements = (struct relem*)malloc(size * sizeof(struct relem));

    return ret;
}

void append_relem(struct relements* relements, struct relem element) {
    if (relements->length >= relements->size) {
        int new_size = relements->size + RELEM_GROW_AMOUNT;

        relements->elements = realloc(relements->elements, new_size * sizeof(struct relem));
        relements->size = new_size;
    }

    relements->elements[relements->length] = element;
    relements->length++;
}

void append_relem_t(struct relements* relements, char type, int symbol) {
    struct relem element;

    element.type = type;

    if (type == REL_SYM) {
        element.sym = symbol & 0xff;
    }
    else if (type == REL_OP) {
        element.op = symbol & 0xff;
    }
    else if (type == REL_TERM) {
        element.id = symbol;
    }

    append_relem(relements, element);
}

#define append_relem_symbol(relements, symbol) append_relem_t(relements, REL_SYM, symbol)
#define append_relem_operator(relements, operator) append_relem_t(relements, REL_OP, operator)

void destroy_relements(struct relements* relements) {
    if (relements->size > 0) {
        free(relements->elements);
    }
    free(relements);
}

void dump_relements(struct relements* relements) {
    int i;
    struct relem* node;

    for (i = 0; i < relements->length; i++) {
        node = &relements->elements[i];

        if (node->type == REL_OP) {
            fprintf(stdout, "%c ", node->op);
        }
        else if (node->type == REL_SYM) {
            fprintf(stdout, "'%c' ", node->sym);
        }
        else if (node->type == REL_TERM) {
            fprintf(stdout, " #(%d)", node->id);
        }
    }

    fprintf(stdout, "\n");
}

struct re_tree* create_re_tree(struct relem* element, struct re_tree* left, struct re_tree *right) {
    struct re_tree *ret = NULL;

    ret = (struct re_tree*)malloc(sizeof(struct re_tree));

    ret->element = (*element);
    ret->left = left;
    ret->right = right;

    ret->id = -1;
    ret->number = -1;

    return ret;
}

void destroy_tree(struct re_tree* tree) {
    if (tree->left) {
        destroy_tree(tree->left);
    }
    if (tree->right) {
        destroy_tree(tree->right);
    }
    free(tree);
}

void dump_tree_recursive(struct re_tree* tree, int indent) {
    int i, j;

    struct re_tree* children[2] = { tree->left, tree->right };

    for (i = 0; i < 2; i++) {
        if (children[i] != NULL) {
            for (j = 0; j < indent; j++) {
                fprintf(stdout, " ");
            }

            if (children[i]->element.type == REL_SYM) {
                fprintf(stdout, "+--- '%c'", children[i]->element.sym);
            }
            else if (children[i]->element.type == REL_OP) {
                fprintf(stdout, "+--- %c", children[i]->element.op);
            }
            else if (children[i]->element.type == REL_TERM) {
                fprintf(stdout, "+--- #(%d)", children[i]->element.id);
            }

            if (children[i]->number >= 0) {
                fprintf(stdout, " <%d>", children[i]->number);
            }

            fprintf(stdout, "\n");

            dump_tree_recursive(children[i], indent + 4);
        }
    }
}

void dump_tree(struct re_tree* tree) {
    dump_tree_recursive(tree, 0);
}

void calculate_reunion(int *dst, int *src1, int *src2, int size) {
    int i;

    for (i = 0; i < size; i++) {
        if ((src1 != NULL && src1[i] != -1) || (src2 != NULL && src2[i] != -1)) {
            dst[i] = i;
        }
    }
}

void node_attr_init(struct re_cc *cc) {
    struct re_node_attr *tmp;
    int i;

    tmp = malloc((cc->num_nodes + 1) * sizeof(struct re_node_attr));
    for (i = 0; i < cc->num_nodes; i++) {
        tmp[i].leaf_number = -1;
        tmp[i].nullable = 0;
        tmp[i].final_id = -1;

        memset(&tmp[i].symbol, 0, sizeof(tmp[i].symbol));

        tmp[i].firstpos = (int*)malloc(cc->num_leafs * sizeof(int));
        memset(tmp[i].firstpos, -1, cc->num_leafs * sizeof(int));

        tmp[i].lastpos = (int*)malloc(cc->num_leafs * sizeof(int));
        memset(tmp[i].lastpos, -1, cc->num_leafs * sizeof(int));
    }

    cc->node_attr = tmp;

    cc->followpos = (int**)malloc(cc->num_leafs * sizeof(int*));
    for (i = 0; i < cc->num_leafs; i++) {
        cc->followpos[i] = (int*)malloc(cc->num_leafs * sizeof(int));
        memset(cc->followpos[i], -1, cc->num_leafs * sizeof(int));
    }
}

void node_attr_destroy(struct re_cc *cc) {
    int i;

    if (cc->node_attr != NULL) {
        for (i = 0; i < cc->num_nodes; i++) {
            free(cc->node_attr[i].firstpos);
            free(cc->node_attr[i].lastpos);
        }

        free(cc->node_attr);
        cc->node_attr = NULL;
    }

    if (cc->followpos != NULL) {
        for (i = 0; i < cc->num_leafs; i++) {
            free(cc->followpos[i]);
        }
        free(cc->followpos);
        cc->followpos = NULL;
    }
}

void dump_tree_attributes(struct re_cc *cc) {
    int i;

    for (i = 0; i < cc->num_leafs; i++) {
        fprintf(stdout, "%2d: ", i);

        int j;
        for (j = 0; j < cc->num_leafs; j++) {
            if (cc->followpos[i][j] == -1) {
                continue;
            }

            fprintf(stdout, "%d ", j);
        }

        fprintf(stdout, "\n");
    }

    fprintf(stdout, "first: ");
    for (i = 0; i < cc->num_leafs; i++) {
        if (cc->node_attr[0].firstpos[i] == -1) {
            continue;
        }

        fprintf(stdout, "%d ", i);
    }
    fprintf(stdout, "\n");

    fprintf(stdout, "last: ");
    for (i = 0; i < cc->num_leafs; i++) {
        if (cc->node_attr[0].lastpos[i] == -1) {
            continue;
        }

        fprintf(stdout, "%d ", i);
    }
    fprintf(stdout, "\n");
}

void dump_fsm(struct re_cc *cc) {
    int i;

    for (i = 0; i < cc->num_leafs; i++) {
        int c;

        for (c = 0; c < 256; c++) {
            if (cc->fsm[i].next_state[c] != -1) {
                fprintf(stdout, "%3d: '%c' => %3d\n", i, c, cc->fsm[i].next_state[c]);
            }
        }

        if (cc->fsm[i].final_id != -1) {
            fprintf(stdout, "\t %d ==> Generates token %d\n", i, cc->fsm[i].final_id);
        }
    }
}

void cc_init(struct re_cc *cc) {
    cc->re_elements = NULL;
    cc->re_tree = NULL;
    cc->node_attr = NULL;
    cc->num_nodes = 0;
    cc->num_leafs = 0;
    cc->followpos = NULL;

    cc->fsm = NULL;
    cc->num_states = 0;

    cc->error = 0;
}

void cc_destroy(struct re_cc *cc) {
    if (cc->re_elements != NULL) {
        destroy_relements(cc->re_elements);
        cc->re_elements = NULL;
    }

    if (cc->re_tree != NULL) {
        destroy_tree(cc->re_tree);
        cc->re_tree = NULL;
    }

    node_attr_destroy(cc);

    if (cc->fsm != NULL) {
        free(cc->fsm);
        cc->fsm = NULL;
    }
}

inline int is_operator(char symbol) {
    /* '.' is just an internal operator */
    switch (symbol) {
        case '*':
        case '+':
        case '?':
        case '|':
        case '(':
        case ')':
            return 1;
    }

    return 0;
}

inline int operator_precedence(char op) {
    switch (op) {
        case '|':
            return 1;

        case '.':
            return 2;

        case '*':
        case '?':
        case '+':
            return 3;
    }

    return 0;
}

int escaped_character(const char *input, int *i, char *ret) {
    char c = input[*i];

    switch (c) {
        case 'n':
            (*ret) = '\n';
            break;

        case 'r':
            (*ret) = '\r';
            break;

        case 't':
            (*ret) = '\t';
            break;

        case 'f':
            (*ret) = '\f';
            break;

        case 'x':
            if (input[(*i) + 1] == 0 || input[(*i) + 2] == 0) {
                /* Unexpected end of input */
                (*ret) = 'x';
                break;
            }
            else {
                char c1, c2;
                char n1, n2;

                c1 = input[(*i) + 1];
                c2 = input[(*i) + 2];

                if ('0' <= c1 && c1 <= '9') {
                    n1 = c1 - '0';
                }
                else if ('a' <= c1 && c1 <= 'f') {
                    n1 = c1 - 'a' + 10;
                }
                else if ('A' <= c1 && c1 <= 'F') {
                    n1 = c1 - 'A' + 10;
                }
                else {
                    (*ret) = 'x';
                    break;
                }

                if ('0' <= c2 && c2 <= '9') {
                    n2 = c2 - '0';
                }
                else if ('a' <= c2 && c2 <= 'f') {
                    n2 = c2 - 'a' + 10;
                }
                else if ('A' <= c2 && c2 <= 'F') {
                    n2 = c2 - 'A' + 10;
                }
                else {
                    (*ret) = 'x';
                    break;
                }

                (*ret) = (n1 << 4) | n2;
                (*i) += 2;
            }
            break;

        /* TODO: there are more character escapes */

        default:
            (*ret) = c;
            break;
    }

    return 0;
}


/* Regex compiler methods */

struct relements *convert_to_regex_elements(const char *input, int identifier) {
    struct relements* relements = NULL;
    char c, last_char = 0, escaped_char;
    int input_len = strlen(input);
    char multi_symbols[256] = { 0 };
    int i, j;
    int state;
    int mode_escape, mode_multisym_range, mode_negative_multisym;

    relements = create_relements(input_len);

    #define S_NORMAL                0
    #define S_MULTISYM              1

    state = S_NORMAL;
    mode_escape = 0;
    mode_multisym_range = 0;
    mode_negative_multisym = 0;

    append_relem_operator(relements, '(');

    for (i = 0; i < input_len; i++) {
        c = input[i];

        if (state == S_NORMAL) {
            if (mode_escape) {
                escaped_character(input, &i, &escaped_char);
                append_relem_symbol(relements, escaped_char);
                mode_escape = 0;
                continue;
            }

            if (c == '\\') {
                mode_escape = 1;
                continue;
            }

            /* Look-ahead */
            if (c == '^') {
                if (input[i + 1] != '\0' && input[i + 1] == '[') {
                    state = S_MULTISYM;
                    memset(&multi_symbols, 0, sizeof(multi_symbols));
                    mode_negative_multisym = 1;
                    continue;
                }
            }

            if (c == '[') {
                state = S_MULTISYM;
                memset(&multi_symbols, 0, sizeof(multi_symbols));
                mode_negative_multisym = 0;
                continue;
            }

            if (is_operator(c)) {
                append_relem_operator(relements, c);
            }
            else {
                append_relem_symbol(relements, c);
            }
        }
        else if (state == S_MULTISYM) {
            if (mode_escape) {
                escaped_character(input, &i, &escaped_char);
                multi_symbols[(int)escaped_char] = escaped_char;
                last_char = escaped_char;
                mode_escape = 0;
                continue;
            }

            if (c == '\\') {
                mode_escape = 1;
                continue;
            }

            if (c == ']') {
                append_relem_operator(relements, '(');
                int first = 1;
                for (j = 0; j < 256; j++) {
                    if (multi_symbols[j] != (mode_negative_multisym ? j : 0)) {
                        if (first == 0) {
                            append_relem_operator(relements, '|');
                        }
                        else {
                            first = 0;
                        }

                        append_relem_symbol(relements, (char)j);
                    }
                }
                append_relem_operator(relements, ')');

                state = S_NORMAL;

                continue;
            }
            else if (c == '-') {
                mode_multisym_range = 1;
                continue;
            }

            if (mode_multisym_range == 1) {
                for (j = (int)last_char; j < (int)c; j++) {
                    multi_symbols[j] = j;
                }

                mode_multisym_range = 0;
            }

            multi_symbols[(int)c] = c;
            last_char = c;
        }
    }

    append_relem_operator(relements, ')');
    append_relem_t(relements, REL_TERM, identifier);

    return relements;
}

struct relements *tokens_to_regex_elements(struct re_token_definition* definitions) {
    struct re_token_definition *tmp;
    struct relements *relements = NULL, *tmp_re = NULL;
    int k, i;

    relements = create_relements(10);

    k = 0;
    tmp = &definitions[k++];
    while (tmp->regex != NULL) {
        tmp_re = convert_to_regex_elements(tmp->regex, tmp->id);

        append_relem_operator(relements, '(');

        for (i = 0; i < tmp_re->length; i++) {
            append_relem(relements, tmp_re->elements[i]);
        }
        destroy_relements(tmp_re);
        tmp_re = NULL;

        append_relem_operator(relements, ')');
        if (definitions[k].regex != NULL) {
            append_relem_operator(relements, '|');
        }

        tmp = &definitions[k++];
    }

    return relements;
}

int add_concatenations(struct re_cc *cc) {
    int ret = -1;
    struct relem *elements = cc->re_elements->elements;
    int length = cc->re_elements->length;
    int i, add_concat;
    struct relements* relements = NULL;
    struct relem *a, *b;

    relements = create_relements(length);

    append_relem(relements, elements[0]);
    for (i = 1; i < length; i++) {
        a = &elements[i - 1];
        b = &elements[i];

        add_concat = 1;

        if ((a->type == REL_OP && a->op == '(') || (b->type == REL_OP && b->op == ')')) {
            add_concat = 0;
        }
        else if ((a->type == REL_OP && a->op == ')') && b->type == REL_OP && b->op != '(') {
            add_concat = 0;
        }

        if (b->type == REL_OP) {
            switch (b->op) {
                case '?':
                case '+':
                case '*':
                case '|':
                    add_concat = 0;
                    break;
            }
        }

        if (a->type == REL_OP) {
            switch (a->op) {
                case '|':
                    add_concat = 0;
                    break;
            }
        }

        if (add_concat == 1) {
            append_relem_operator(relements, '.');
        }
        append_relem(relements, (*b));
    }

    /* Success */
    destroy_relements(cc->re_elements);
    cc->re_elements = relements;
    ret = 0;

    return ret;
}

int build_tree(struct relem** stiva_op, int* stiva_op_l, struct re_tree** stiva_t, int* stiva_t_l) {
    struct relem* op;
    struct re_tree *S, *D;

    if ((*stiva_op_l) < 1) {
        /* Internal error ? */
        return RE_ERROR_UNKNOWN;
    }

    if ((*stiva_t_l) < 1) {
        /* Operator with no operand */
        return RE_ERROR_OPERATOR_WITH_NO_OPERAND;
    }

    op = stiva_op[--(*stiva_op_l)];
    D = stiva_t[--(*stiva_t_l)];

    switch (op->sym) {
        case '*':
        case '+':
        case '?':
            stiva_t[(*stiva_t_l)++] = create_re_tree(op, D, NULL);
            break;

        case '|':
        case '.':
            if ((*stiva_t_l) < 1) {
                stiva_t[(*stiva_t_l)++] = D;
                stiva_op[(*stiva_op_l)++] = op;
                /* Operator with no operand */
                return RE_ERROR_OPERATOR_WITH_NO_OPERAND;
            }
            S = stiva_t[--(*stiva_t_l)];
            stiva_t[(*stiva_t_l)++] = create_re_tree(op, S, D);
            break;

        default:
            stiva_t[(*stiva_t_l)++] = D;
            stiva_op[(*stiva_op_l)++] = op;
            /* Internal error ? */
            return RE_ERROR_UNKNOWN;
    }

    return RE_ERROR_NONE;
}

int build_re_tree(struct re_cc *cc) {
    int ret = -1;
    struct relements *input = cc->re_elements;
    struct re_tree* tree = NULL;
    int i, rc;
    struct relem* element;

    // struct relem* stiva_op[input->length + 2];
    struct relem** stiva_op = malloc((input->length + 2) * sizeof(struct relem*));
    int stiva_op_l = 0;

    // struct re_tree* stiva_t[input->length + 2];
    struct re_tree** stiva_t = malloc((input->length + 2) * sizeof(struct re_tree*));
    int stiva_t_l = 0;

    for (i = 0; i < input->length; i++) {
        element = &input->elements[i];

        if (element->type == REL_SYM || element->type == REL_TERM) {
            stiva_t[stiva_t_l++] = create_re_tree(element, NULL, NULL);
        }
        else {
            switch (element->op) {
                case '(':
                    stiva_op[stiva_op_l++] = element;
                    break;

                case ')':
                    if (stiva_op_l > 0) {
                        while (stiva_op[stiva_op_l - 1]->op != '(') {
                            rc = build_tree(stiva_op, &stiva_op_l, stiva_t, &stiva_t_l);
                            if (rc != RE_ERROR_NONE) {
                                cc->error = rc;
                                goto _error;
                            }
                        }

                        if (stiva_op[stiva_op_l - 1]->op != '(') {
                            /* Error: Unmatched paranthesis */
                            cc->error = RE_ERROR_UNMATCHED_PARANTHESIS;
                            goto _error;
                        }

                        stiva_op_l--;
                    }
                    else {
                        /* Error: Unmatched paranthesis */
                        cc->error = RE_ERROR_UNMATCHED_PARANTHESIS;
                        goto _error;
                    }
                    break;

                default:
                    while (stiva_op_l > 0 &&
                                operator_precedence(stiva_op[stiva_op_l - 1]->op) >= operator_precedence(element->op)) {
                        rc = build_tree(stiva_op, &stiva_op_l, stiva_t, &stiva_t_l);

                        if (rc != RE_ERROR_NONE) {
                            cc->error = rc;
                            goto _error;
                        }
                    }
                    stiva_op[stiva_op_l++] = element;
                    break;
            }
        }
    }

    while (stiva_op_l > 0) {
        rc = build_tree(stiva_op, &stiva_op_l, stiva_t, &stiva_t_l);

        if (rc != RE_ERROR_NONE) {
            cc->error = rc;
            goto _error;
        }
    }

    if (stiva_t_l < 1) {
        /* Internal error ? */
        return RE_ERROR_UNKNOWN;
    }

    tree = stiva_t[--stiva_t_l];

    /* Success */
    cc->re_tree = tree;
    ret = 0;
    goto _end;

_error:
    ret = -1;

_end:
    /* Cleanup */
    while (stiva_t_l > 0) {
        destroy_tree(stiva_t[--stiva_t_l]);
    }
    return ret;
}

int calculate_attributes_recursive(struct re_cc *cc, struct re_tree *tree) {
    struct re_node_attr *node_attr = cc->node_attr;
    int num_leafs = cc->num_leafs;
    int id = tree->id;

    node_attr[id].final_id = -1;

    if (tree->element.type == REL_SYM) {
        node_attr[id].leaf_number = tree->number;
        node_attr[id].nullable = 0;
        node_attr[id].symbol = tree->element;
        node_attr[id].firstpos[tree->number] = tree->number;
        node_attr[id].lastpos[tree->number] = tree->number;
    }
    else if (tree->element.type == REL_OP) {
        struct re_node_attr *c1 = NULL, *c2 = NULL;

        switch (tree->element.op) {
            case '|':
            case '.':
                calculate_attributes_recursive(cc, tree->left);
                calculate_attributes_recursive(cc, tree->right);
                c1 = &node_attr[tree->left->id];
                c2 = &node_attr[tree->right->id];
                break;

            case '*':
            case '?':
            case '+':
                calculate_attributes_recursive(cc, tree->left);
                c1 = &node_attr[tree->left->id];
                break;
        }

        switch (tree->element.op) {
            case '|':
                node_attr[id].nullable = c1->nullable | c2->nullable;
                calculate_reunion(node_attr[id].firstpos, c1->firstpos, c2->firstpos, num_leafs);
                calculate_reunion(node_attr[id].lastpos, c1->lastpos, c2->lastpos, num_leafs);
                break;

            case '.':
                node_attr[id].nullable = c1->nullable & c2->nullable;

                if (c1->nullable) {
                    calculate_reunion(node_attr[id].firstpos, c1->firstpos, c2->firstpos, num_leafs);
                }
                else {
                    calculate_reunion(node_attr[id].firstpos, c1->firstpos, NULL, num_leafs);
                }

                if (c2->nullable) {
                    calculate_reunion(node_attr[id].lastpos, c1->lastpos, c2->lastpos, num_leafs);
                }
                else {
                    calculate_reunion(node_attr[id].lastpos, NULL, c2->lastpos, num_leafs);
                }

                break;

            case '*':
            case '?':
                node_attr[id].nullable = 1;
                calculate_reunion(node_attr[id].firstpos, c1->firstpos, NULL, num_leafs);
                calculate_reunion(node_attr[id].lastpos, c1->lastpos, NULL, num_leafs);
                break;

            case '+':
                node_attr[id].nullable = c1->nullable;
                calculate_reunion(node_attr[id].firstpos, c1->firstpos, NULL, num_leafs);
                calculate_reunion(node_attr[id].lastpos, c1->lastpos, NULL, num_leafs);
                break;
        }
    }
    else if (tree->element.type == REL_TERM) {
        node_attr[id].leaf_number = tree->number;
        node_attr[id].nullable = 1;
        node_attr[id].final_id = tree->element.id;
        node_attr[id].firstpos[tree->number] = tree->number;
        node_attr[id].lastpos[tree->number] = tree->number;
    }

    return 0;
}

int calculate_followpos_recursive(struct re_cc *cc, struct re_tree *node) {
    int id = node->id;
    int i;

    if (node->left) {
        calculate_followpos_recursive(cc, node->left);
    }
    if (node->right) {
        calculate_followpos_recursive(cc, node->right);
    }

    if (node->element.type == REL_OP) {
        switch (node->element.op) {
            case '.':
                for (i = 0; i < cc->num_leafs; i++) {
                    if (cc->node_attr[node->left->id].lastpos[i] == -1) {
                        continue;
                    }

                    calculate_reunion(cc->followpos[i], cc->node_attr[node->right->id].firstpos, NULL, cc->num_leafs);
                }
                break;

            case '*':
            case '+':
                for (i = 0; i < cc->num_leafs; i++) {
                    if (cc->node_attr[id].lastpos[i] == -1) {
                        continue;
                    }

                    calculate_reunion(cc->followpos[i], cc->node_attr[id].firstpos, NULL, cc->num_leafs);
                }
                break;
        }
    }

    return 0;
}

int generate_states(struct re_cc *cc) {
    int num_leafs = cc->num_leafs;
    int i, j, k, new_state_id;
    int c;
    int **nstates = NULL;
    int nstates_l = 0;
    int *new_state;
    struct re_node_attr **state_attr;

    nstates = (int**)matrix_allocate(num_leafs + 1, num_leafs + 1, sizeof(int));
    new_state = malloc(num_leafs * sizeof(int));
    state_attr = malloc((num_leafs + 1) * sizeof(struct re_node_attr*));

    for (i = 0; i < cc->num_nodes; i++) {
        if (cc->node_attr[i].leaf_number > -1) {
            state_attr[cc->node_attr[i].leaf_number] = &cc->node_attr[i];
        }
    }

    for (i = 0; i < num_leafs; i++) {
        memset(nstates[i], -1, num_leafs * sizeof(int));
    }

    calculate_reunion(nstates[nstates_l++], cc->node_attr[0].firstpos, NULL, num_leafs);

    for (i = 0; i < nstates_l; i++) {
        int final_id = -1;

        for (c = -1; c < 256; c++) {
            int found = 0;
            memset(new_state, -1, num_leafs * sizeof(int));

            for (j = 0; j < num_leafs; j++) {
                if (nstates[i][j] == -1) {
                    continue;
                }

                int state_nr = nstates[i][j];

                if (c == -1) {
                    if (state_attr[state_nr]->symbol.type == REL_TERM) {
                        if (state_attr[state_nr]->final_id != -1) {
                            /* Give priority to the smaller numbered token */
                            if (final_id == -1 || final_id > state_attr[state_nr]->final_id) {
                                final_id = state_attr[state_nr]->final_id;

                                found = 1;
                                new_state[state_nr] = state_nr;
                            }
                        }
                    }
                }
                else {
                    if (state_attr[state_nr]->symbol.type != REL_SYM) {
                        continue;
                    }
                    if (state_attr[state_nr]->symbol.sym != (char)c) {
                        continue;
                    }

                    found = 1;
                    calculate_reunion(new_state, cc->followpos[nstates[i][j]], NULL, num_leafs);
                }
            }

            if (found == 1) {
                found = 0;
                for (j = 0; j < nstates_l; j++) {
                    for (k = 0; k < num_leafs; k++) {
                        if (nstates[j][k] != new_state[k]) {
                            break;
                        }
                    }

                    if (k == num_leafs) {
                        new_state_id = j;
                        found = 1;
                        break;
                    }
                }

                if (found == 0) {
                    calculate_reunion(nstates[nstates_l++], new_state, NULL, num_leafs);
                    new_state_id = nstates_l - 1;
                }
                cc->fsm[i].next_state[(int)c] = new_state_id;
                cc->num_states++;
                if (final_id != -1) {
                    cc->fsm[i].final_id = final_id;
                }
            }
        }
    }

fprintf(stderr, "%d states generated\n", cc->num_states);

    matrix_destroy((void**)nstates, num_leafs + 1, num_leafs + 1);
    free(new_state);
    free(state_attr);

    return 0;
}


int re_compile_from_elements(struct relements *re_elements, struct re_fsm_state **fsm, int* num_states) {
    int ret = -1;
    struct re_cc cc;
    struct re_tree *node = NULL;
    struct re_stack tree_visit_stack;

    cc_init(&cc);

    cc.re_elements = re_elements;

    /* Add the concatenators symbol where it's needed */
    add_concatenations(&cc);

#ifdef DEBUG
    dump_relements(cc.re_elements);
#endif

    /* Build expression tree */
    if (build_re_tree(&cc) != 0) {
        /* TODO: Some error */
        goto _error;
    }

    /* Number tree nodes */
    stack_init(&tree_visit_stack, cc.re_elements->length + 1);
    stack_push(&tree_visit_stack, cc.re_tree);
    while (stack_level(&tree_visit_stack) > 0) {
        node = stack_pop(&tree_visit_stack);

        node->id = cc.num_nodes++;

        if (node->right == NULL && node->left == NULL) {
            node->number = cc.num_leafs++;
        }

        if (node->left) {
            stack_push(&tree_visit_stack, node->left);
        }
        if (node->right) {
            stack_push(&tree_visit_stack, node->right);
        }
    }
    stack_destroy(&tree_visit_stack);

    /* Compute transitions */
    node_attr_init(&cc);

    calculate_attributes_recursive(&cc, cc.re_tree);

    calculate_followpos_recursive(&cc, cc.re_tree);

#ifdef DEBUG
    dump_tree_attributes(&cc);
    dump_tree(cc.re_tree);
#endif

    cc.fsm = (struct re_fsm_state*)malloc(cc.num_leafs * sizeof(struct re_fsm_state));
    memset(&cc.fsm[0], -1, cc.num_leafs * sizeof(struct re_fsm_state));

    generate_states(&cc);

#ifdef DEBUG
    dump_fsm(&cc);
#endif

    /* ... */
    (*fsm) = cc.fsm;
    (*num_states) = cc.num_states;
    cc.fsm = NULL;

    /* Success */
    ret = 0;
    goto _end;

_error:
    ret = -1;

_end:
    /* Clean up */
    node_attr_destroy(&cc);
    cc_destroy(&cc);
    return ret;
}

int re_compile(const char *input, struct re_fsm_state **fsm, int* num_states) {
    struct relements *re_elements = NULL;

    /* Separate symbols and operators (handle escapes, multi symbols, etc.) */
    re_elements = convert_to_regex_elements(input, 0);

    return re_compile_from_elements(re_elements, fsm, num_states);
}

int re_compile_tokens(struct re_token_definition *definitions, struct re_fsm_state **fsm, int *num_states) {
    struct relements *re_elements = NULL;

    /* Separate symbols and operators (handle escapes, multi symbols, etc.) */
    re_elements = tokens_to_regex_elements(definitions);

    return re_compile_from_elements(re_elements, fsm, num_states);
}

