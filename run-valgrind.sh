#!/bin/bash


# RUN_VALGRIND="valgrind --tool=memcheck --leak-check=yes --show-reachable=yes --time-stamp=yes --log-file=valgrind.log --vgdb=full --db-attach=yes"
RUN_VALGRIND="valgrind --tool=memcheck --leak-check=yes --show-reachable=yes --time-stamp=yes --vgdb=full --db-attach=yes"

$RUN_VALGRIND ./lexer-generator input00.txt

