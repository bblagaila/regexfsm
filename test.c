#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>


#include "regex_compiler.h"
#include "lexer.h"

struct user_data {
    FILE* file;
    char read_char;
    int eof;
};

int cb_lookahead(void *u_data, char* c) {
    struct user_data *user_data = (struct user_data*)u_data;

    if (user_data->eof) {
        return -1;
    }

    (*c) = user_data->read_char;

    return 0;
}

void cb_consume(void *u_data) {
    struct user_data *user_data = (struct user_data*)u_data;
    int c;

    c = fgetc(user_data->file);
    if (c == EOF) {
        user_data->read_char = 0;
        user_data->eof = 1;
    }
    else {
        user_data->read_char = c & 0xff;
    }
}

int open_file(const char *filename, struct user_data **u_data) {
    struct user_data *user_data = NULL;

    (*u_data) = NULL;

    user_data = (struct user_data*)malloc(sizeof(struct user_data));

    user_data->file = fopen(filename, "rb");
    if (user_data->file == NULL) {
        fprintf(stderr, "Cannot open file <%s>: %s", filename, strerror(errno));
        free(user_data);
        return -1;
    }

    user_data->eof = 0;
    cb_consume(user_data);

    (*u_data) = user_data;

    return 0;
}

void close_file(struct user_data *user_data) {
    if (user_data->file != NULL) {
        fclose(user_data->file);
    }
    free(user_data);
}

void parse_file(const char* filename, struct re_token_definition *tokens) {
    int rc;
    struct lexer_context *lexer = NULL;
    struct user_data *user_data = NULL;
    struct lexer_token token;

    if (open_file(filename, &user_data) != 0) {
        return;
    }

    lexer = lexer_context_create(cb_lookahead, cb_consume, user_data);

    lexer_define_tokens(lexer, tokens);

    fprintf(stdout, "Begin parsing: %s\n", filename);
    while (1) {
        rc = lexer_next_token(lexer, &token);
        if (rc != 0) {
            break;
        }

        /* EOF ? */
        if (token.id == LEXER_TOKEN_EOF) {
            fprintf(stdout, " <<EOF >>\n");
            break;
        }

        if (token.id >= 99) {
            /* Ignore whitespace */
            continue;
        }

        fprintf(stdout, "Token ID %2d [%d:%d]: %s\n", token.id, token.line, token.column, token.content);
    }
    fprintf(stdout, "Finished parsing.\n");

    close_file(user_data);
}

int main(int argc, char **argv) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s <input>\n", argv[0]);
        return 1;
    }

#if 1
    enum TokenTypes {
        T_NONE = 0,
        T_COLON,
        T_SEMICOLON,
        T_FRAGMENT,
        T_IDENTIFIER,
        T_STRING,
        T_REGEX_RANGE,
        T_REGEX_OPERATOR,

        T_WHITESPACE = 99,
        T_COMMENT = 100
    };

    struct re_token_definition tokens[] = {
        { T_COLON,          "\\:" },
        { T_SEMICOLON,      "\\;" },
        { T_FRAGMENT,       "fragment" },
        { T_IDENTIFIER,     "[A-Za-z_][A-Za-z_0-9]*", },
        { T_STRING,         "'(^['\\\\]|\\\\(\\\\|'|a|b|f|n|r|t|u|x))*'", },
        { T_REGEX_RANGE,    "\\^?\\[((^[\\[\\]])|(\\\\[|\\\\]))*\\]", },
        { T_REGEX_OPERATOR, "(\\|)|(\\*)|(\\?)|(\\+)|(\\()|(\\))" },

        { T_WHITESPACE, "[\\ \\t\\n\\r]*" },
        { T_COMMENT, "/((/(^[\n\r])*)|(\\*(^[\\*]|\\*^[/])*\\*/))" },

        { -1, NULL },
    };
#else
    const char *TOKENS[] = {
        "if", "for", "foreach", "while",
        "{", "}",
        "\\(", "\\)",
        "=", "\\+", ";",
        "null", "undefined",
        "-?[1-9][0-9]*(\\.[0-9][0-9]*)?",
        "[A-Za-z_][A-Za-z_0-9]*",
        NULL
    };

    int k = 0, length;
    while (TOKENS[k] != NULL) { k++; }

    length = k;
    struct re_token_definition *tokens = malloc((length + 2) * sizeof(struct re_token_definition));
    for (k = 0; k < length; k++) {
        tokens[k].regex = TOKENS[k];
        tokens[k].id = k + 1;
    }
    tokens[k].regex = "[\\ \\t\\n\\r]*";
    tokens[k].id = 99;
    tokens[k+1].regex = NULL;
#endif

    parse_file(argv[1], tokens);

    return 0;
}

