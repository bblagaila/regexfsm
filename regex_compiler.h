#ifndef __REGEX_COMPILER_H__
#define __REGEX_COMPILER_H__


struct re_fsm_state {
    int final_id;
    int next_state[256];
};

struct re_token_definition {
    int id;
    const char *regex;
};

enum re_errors {
    RE_ERROR_NONE = 0,
    RE_ERROR_UNMATCHED_PARANTHESIS,
    RE_ERROR_OPERATOR_WITH_NO_OPERAND,
    RE_ERROR_UNKNOWN
};


/* Methods */

int re_compile(const char *input, struct re_fsm_state **fsm, int* num_states);

int re_compile_tokens(struct re_token_definition *definitions, struct re_fsm_state **fsm, int *num_states);

#endif /* __REGEX_COMPILER_H__ */

