#ifndef __LEXER_H__
#define __LEXER_H__

#include "regex_compiler.h"

#define LEXER_TOKEN_EOF       -1

struct lexer_context {
    struct re_fsm_state *states;
    int num_states;

    int line;
    int column;

    /* Callbacks */
    int (*lookahead)(void *user_data, char* character);
    void (*consume)(void *user_data);

    void *user_data;
};

struct lexer_token {
    int id;
    char *content;
    int line;
    int column;
};


/* Methods */

struct lexer_context *lexer_context_create(int (*lookahead)(void*, char*), void (*consume)(void*), void* user_data);

int lexer_define_tokens(struct lexer_context *context, struct re_token_definition *definitions);

int lexer_next_token(struct lexer_context *context, struct lexer_token *token);


#endif /* __LEXER_H__ */

