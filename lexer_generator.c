#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "regex_compiler.h"
#include "lexer.h"

enum TokenTypes {
    T_NONE = 0,
    T_COLON,
    T_SEMICOLON,
    T_FRAGMENT,
    T_IDENTIFIER,
    T_STRING,
    T_REGEX_RANGE,
    T_REGEX_OPERATOR,

    T_IGNORED_TOKENS = 99,
    T_WHITESPACE = 100,
    T_COMMENT = 101
};

char *token_names[] = {
    "T_NONE",
    "T_COLON",
    "T_SEMICOLON",
    "T_FRAGMENT",
    "T_IDENTIFIER",
    "T_STRING",
    "T_REGEX_RANGE",
    "T_REGEX_OPERATOR",
    NULL
};

struct re_token_definition tokens[] = {
    { T_COLON,          "\\:" },
    { T_SEMICOLON,      "\\;" },
    { T_FRAGMENT,       "fragment" },
    { T_IDENTIFIER,     "[A-Za-z_][A-Za-z_0-9]*", },
    { T_STRING,         "'(^['\\\\]|\\\\(\\\\|'|a|b|f|n|r|t|u|x))*'", },
    { T_REGEX_RANGE,    "\\^?\\[((^[\\[\\]])|(\\\\[|\\\\]))*\\]", },
    { T_REGEX_OPERATOR, "(\\|)|(\\*)|(\\?)|(\\+)|(\\()|(\\))" },

    { T_WHITESPACE, "[\\ \\t\\n\\r]*" },
    { T_COMMENT, "/((/(^[\n\r])*)|(\\*(^[\\*]|\\*^[/])*\\*/))" },

    { -1, NULL },
};

struct user_data {
    FILE* file;
    char read_char;
    int eof;
};

int cb_lookahead(void *u_data, char* c) {
    struct user_data *user_data = (struct user_data*)u_data;

    if (user_data->eof) {
        return -1;
    }

    (*c) = user_data->read_char;

    return 0;
}

void cb_consume(void *u_data) {
    struct user_data *user_data = (struct user_data*)u_data;
    int c;

    c = fgetc(user_data->file);
    if (c == EOF) {
        user_data->read_char = 0;
        user_data->eof = 1;
    }
    else {
        user_data->read_char = c & 0xff;
    }
}

int open_file(const char *filename, struct user_data **u_data) {
    struct user_data *user_data = NULL;

    (*u_data) = NULL;

    user_data = (struct user_data*)malloc(sizeof(struct user_data));

    user_data->file = fopen(filename, "rb");
    if (user_data->file == NULL) {
        fprintf(stderr, "Cannot open file <%s>: %s", filename, strerror(errno));
        free(user_data);
        return -1;
    }

    user_data->eof = 0;
    cb_consume(user_data);

    (*u_data) = user_data;

    return 0;
}

void close_file(struct user_data *user_data) {
    if (user_data->file != NULL) {
        fclose(user_data->file);
    }
    free(user_data);
}

struct lexer_context* initialize_context(const char *filename) {
    struct lexer_context *lexer = NULL;
    struct user_data *user_data = NULL;

    if (open_file(filename, &user_data) != 0) {
        return NULL;
    }

    lexer = lexer_context_create(cb_lookahead, cb_consume, user_data);

    lexer_define_tokens(lexer, tokens);

    return lexer;
}

void destroy_context(struct lexer_context *context) {
    struct user_data *user_data = (struct user_data*)context->user_data;
    close_file(user_data);
    free(context);
}

void print_parse_error(struct lexer_token *token, const char *message) {
    fprintf(stderr, "Parse error (%d, %d): %s. Token %d: %s.\n",
                token->line, token->column, message, token->id, token->content);
}

struct rule {
    char identifier[128];
    char regex[2048];
};

struct grammar_state {
    char identifier[128];
    char regex[2048];
    int re_k;
    int is_fragment;

    struct rule rules[64];
    int num_rules;

    struct rule fragments[64];
    int num_fragments;
};

struct generated_lexer_token {
    int id;
    char *name;
};

struct generated_lexer {
    struct generated_lexer_token *tokens;
    int num_tokens;
    struct re_fsm_state *states;
    int num_states;
};

void reset_grammar_state(struct grammar_state *state) {
    state->identifier[0] = '\0';
    state->regex[0] = '\0';
    state->re_k = 0;
    state->is_fragment = 0;
}

void rule_define(struct grammar_state *state, const char *identifier, const char *regex, int is_fragment) {
    struct rule current_rule;
    memset(&current_rule, 0, sizeof(current_rule));

    strcpy(current_rule.identifier, identifier);
    strcpy(current_rule.regex, regex);

    if (is_fragment == 0) {
        state->rules[state->num_rules] = current_rule;
        state->num_rules += 1;
    }
    else {
        state->fragments[state->num_fragments] = current_rule;
        state->num_fragments += 1;
    }
}

struct rule* rule_find(struct grammar_state *state, const char *identifier, int is_fragment) {
    int i;
    struct rule* rule_list;
    int num_rules;

    if (is_fragment == 0) {
        rule_list = state->rules;
        num_rules = state->num_rules;
    }
    else {
        rule_list = state->fragments;
        num_rules = state->num_fragments;
    }

    for (i = 0; i < num_rules; i++) {
        if (strcmp(rule_list[i].identifier, identifier) == 0) {
            return &rule_list[i];
        }
    }

    return NULL;
}

struct generated_lexer* generate_lexer(struct grammar_state *state) {
    int rc;
    struct generated_lexer *ret = NULL;
    struct re_token_definition *tokens_regex = NULL;
    int i;

    ret = malloc(sizeof(struct generated_lexer));

    tokens_regex = (struct re_token_definition*)malloc((state->num_rules + 1) * sizeof(struct re_token_definition));
    ret->tokens = (struct generated_lexer_token*)malloc(state->num_rules * sizeof(struct generated_lexer_token));
    for (i = 0; i < state->num_rules; i++) {
        ret->tokens[i].id = i + 1;
        ret->tokens[i].name = strdup(state->rules[i].identifier);

        tokens_regex[i].id = i + 1;
        tokens_regex[i].regex = state->rules[i].regex;
    }
    tokens_regex[i].id = -1;
    tokens_regex[i].regex = NULL;

    ret->num_tokens = state->num_rules;

    rc = re_compile_tokens(tokens_regex, &ret->states, &ret->num_states);
    if (rc != 0) {
        goto _error;
    }

    /* Success */
    goto _end;

_error:
    free(ret->tokens);
    free(ret);
    ret = NULL;
_end:
    free(tokens_regex);
    return ret;
}

void dump_lexer(struct generated_lexer *lexer) {
    int i, j;

    fprintf(stdout, "Tokens:\n");
    for (i = 0; i < lexer->num_tokens; i++) {
        fprintf(stdout, "%d: %s\n", lexer->tokens[i].id, lexer->tokens[i].name);
    }
    fprintf(stdout, "\n");

    fprintf(stdout, "FSM:\n");
    fprintf(stdout, "Nr. states: %d\n", lexer->num_states);
    for (i = 0; i < lexer->num_states; i++) {
        fprintf(stdout, "* %d ", lexer->states[i].final_id);

        for (j = 0; j < 256; j++) {
            fprintf(stdout, "%d ", lexer->states[i].next_state[j]);
        }

        fprintf(stdout, "\n");
    }
    fprintf(stdout, ".\n");
}

enum grammar_states {
    S_NONE = 0,
    S_INITIAL,
    S_EXPECT_DEFINITION,
    S_DEFINITION,
};

int parse_grammar_definition(struct lexer_context *lexer) {
    int ret = -1;
    int rc, i;
    struct lexer_token token;

    int state;
    struct grammar_state state_info;

    memset(&state_info, 0, sizeof(state_info));
    reset_grammar_state(&state_info);
    state = S_INITIAL;

    while (1) {
        rc = lexer_next_token(lexer, &token);
        if (rc != 0) {
            /* Token parse error */
            goto _error;
        }

        /* EOF ? */
        if (token.id == LEXER_TOKEN_EOF) {
            fprintf(stdout, " << EOF >>\n");
            break;
        }

        /* Ignore whitespace */
        if (token.id >= T_IGNORED_TOKENS) {
            continue;
        }

if (0) {
    fprintf(stdout, "Got token <%20s>[%3d:%3d]: %s\n", token_names[token.id],
                token.line, token.column, token.content);
}

        switch (state) {
            case S_INITIAL:
                if (token.id == T_IDENTIFIER) {
                    strcpy(state_info.identifier, token.content);
                    state = S_EXPECT_DEFINITION;
                }
                else if (token.id == T_FRAGMENT) {
                    state_info.is_fragment = 1;
                }
                else {
                    fprintf(stderr, "Parse error: expected T_IDENTIFIER or T_FRAGMENT. Got: %s.\n", token.content);
                    goto _parse_error;
                }
                break;

            case S_EXPECT_DEFINITION:
                if (token.id == T_COLON) {
                    state = S_DEFINITION;
                }
                else {
                    print_parse_error(&token, "expected T_COLON");
                    goto _parse_error;
                }
                break;

            case S_DEFINITION:
                if (token.id == T_IDENTIFIER) {
                    struct rule *fragment = rule_find(&state_info, token.content, 1);
                    if (fragment == NULL) {
                        print_parse_error(&token, "undefined fragment");
                        goto _parse_error;
                    }

                     /* Merge it to the regular expression */
                    state_info.regex[state_info.re_k++] = '(';

                    /* Ignore beginning and ending quotes */
                    for (i = 0; fragment->regex[i] != 0; i++) {
                        state_info.regex[state_info.re_k++] = fragment->regex[i];
                    }

                    state_info.regex[state_info.re_k++] = ')';
                    state_info.regex[state_info.re_k] = '\0';
                }
                else if (token.id == T_SEMICOLON) {
                    /* Definition ended ... process it */
    fprintf(stdout, "Got rule: \"%s\"", state_info.identifier);
    fprintf(stdout, " with expression: \"%s\".\n", state_info.regex);

                    rule_define(&state_info, state_info.identifier, state_info.regex, state_info.is_fragment);

                    reset_grammar_state(&state_info);
                    state = S_INITIAL;
                }
                else if (token.id == T_STRING) {
                    /* Merge it to the regular expression */
                    state_info.regex[state_info.re_k++] = '(';

                    /* Ignore beginning and ending quotes */
                    for (i = 1; token.content[i + 1] != 0; i++) {
                        char c = token.content[i];

                        switch (c) {
                            case '|':
                            case '*':
                            case '?':
                            case '+':
                            case '(':
                            case ')':
                            case '[':
                            case ']':
                                state_info.regex[state_info.re_k++] = '\\';
                                break;
                        }

                        state_info.regex[state_info.re_k++] = token.content[i];
                    }

                    state_info.regex[state_info.re_k++] = ')';
                    state_info.regex[state_info.re_k] = '\0';
                }
                else if (token.id == T_REGEX_RANGE || token.id == T_REGEX_OPERATOR) {
                    for (i = 0; token.content[i] != 0; i++) {
                        state_info.regex[state_info.re_k++] = token.content[i];
                    }
                    state_info.regex[state_info.re_k] = '\0';
                }
                else {
                    fprintf(stderr, "Parse error: expected T_COLON. Got: %s.\n", token.content);
                    goto _parse_error;
                }
                break;

            default:
                fprintf(stderr, "Internal error: Unknown state.\n");
                return -1;
        }
    }

    struct generated_lexer *gen_lexer = generate_lexer(&state_info);
    dump_lexer(gen_lexer);

    /* Success */
    ret = 0;
    goto _end;

_parse_error:
_error:
    ret = -1;
_end:
    // TODO: cleanup
    return ret;
}

void parse_file(const char* filename, struct re_token_definition *tokens) {
    int rc;
    struct lexer_context *lexer = NULL;
    struct user_data *user_data = NULL;
    struct lexer_token token;

    if (open_file(filename, &user_data) != 0) {
        return;
    }

    lexer = lexer_context_create(cb_lookahead, cb_consume, user_data);

    lexer_define_tokens(lexer, tokens);

    fprintf(stdout, "Begin parsing: %s\n", filename);
    while (1) {
        rc = lexer_next_token(lexer, &token);
        if (rc != 0) {
            break;
        }

        /* EOF ? */
        if (token.id == LEXER_TOKEN_EOF) {
            fprintf(stdout, " <<EOF >>\n");
            break;
        }

        if (token.id >= 99) {
            /* Ignore whitespace */
            continue;
        }

        fprintf(stdout, "Token ID %2d [%d:%d]: %s\n", token.id, token.line, token.column, token.content);
    }
    fprintf(stdout, "Finished parsing.\n");

    close_file(user_data);
}

int main(int argc, char **argv) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s <input>\n", argv[0]);
        return 1;
    }

    struct lexer_context *lexer = NULL;

    lexer = initialize_context(argv[1]);
    if (lexer == NULL) {
        fprintf(stderr, "Cannot open file <%s>: %s\n", argv[1], strerror(errno));
        return 1;
    }

    fprintf(stdout, "Parsing: %s\n", argv[1]);

    parse_grammar_definition(lexer);

    fprintf(stdout, "Done parsing.\n");

    destroy_context(lexer);
    return 0;
}

